package com.webengage.sampleapp;

import android.app.Application;

import com.webengage.sdk.android.WebEngageActivityLifeCycleCallbacks;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(new WebEngageActivityLifeCycleCallbacks(this));
    }
}
