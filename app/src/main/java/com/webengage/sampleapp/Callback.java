package com.webengage.sampleapp;

import android.content.Context;
import android.media.RingtoneManager;
import android.os.Bundle;

import com.webengage.sdk.android.actions.notification.PushNotificationData;
import com.webengage.sdk.android.callbacks.PushNotificationCallbacks;
import com.webengage.sdk.android.utils.WebEngageCallback;

@WebEngageCallback(enabled = true)
public class Callback implements PushNotificationCallbacks {
    @Override
    public PushNotificationData onNotificationReceived(Context context, PushNotificationData notificationData) {
        Bundle customData = notificationData.getCustomData();

        // Enabling sound
        notificationData.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        // Enabling vibration
        notificationData.setVibrateFlag(true);

        return notificationData;
    }

    @Override
    public void onNotificationShown(Context context, PushNotificationData pushNotificationData) {

    }

    @Override
    public void onNotificationClicked(Context context, PushNotificationData pushNotificationData) {

    }

    @Override
    public void onNotificationDismissed(Context context, PushNotificationData pushNotificationData) {

    }

    @Override
    public void onNotificationActionClicked(Context context, PushNotificationData pushNotificationData, String s) {

    }
}

